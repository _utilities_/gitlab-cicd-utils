# GitLab CI/CD Utils

[![pipeline status](https://gitlab.com/_utilities_/gitlab-cicd-utils/badges/main/pipeline.svg)](https://gitlab.com/_utilities_/gitlab-cicd-utils/-/commits/main)
[![Latest Release](https://gitlab.com/_utilities_/gitlab-cicd-utils/-/badges/release.svg)](https://gitlab.com/_utilities_/gitlab-cicd-utils/-/releases)

## Description
This project consists of CI/CD scripts that can be used across different projects in GitLab.

## Scripts

- [gitlab/ci](gitlab/ci) - general scripts.
- [gitlab/ci/maven](gitlab/ci/maven) - scripts related to Maven.

## How to use it?

To use script/scripts in your project you will need to include it in your `.gitlab-ci.yml` 
([see docs about include:project](https://docs.gitlab.com/ee/ci/yaml/#includeproject)). 

Example of use in Maven project:
```yaml
include:
  - project: '_utilities_/gitlab-cicd-utils'
    ref: 0.0.1
    file: 'gitlab/ci/maven/Maven.gitlab-ci.yml'

```