# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Fixed `yaml invalid` `error` - jobs:maven-deploy rules should be an array containing hashes and arrays of hashes

## [0.0.1] - 2022-01-31
### Added
- Init release.
- Added CI/CD scripts for Maven.

[unreleased]: https://gitlab.com/_utilities_/gitlab-cicd-utils/-/compare/0.0.1...main
[0.0.1]: https://gitlab.com/_utilities_/gitlab-cicd-utils/-/tags/0.0.1